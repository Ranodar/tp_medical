package org.example.services;

import org.example.entities.*;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;

import java.util.Date;
import java.util.List;

public class FicheConsultationService extends BaseService implements IDAO<FicheConsultation> {

    public FicheConsultationService(){
        super();
    }

    @Override
    public boolean create(FicheConsultation o) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    public boolean createFicheDeSoin(FicheDeSoin o) {
        session.save(o);
        return true;
    }

    public boolean createConsult(Consultation o) {
        session.save(o);
        return true;
    }

    public boolean createPrescription(Prescription o) {
        session.save(o);
        return true;
    }

    public boolean createOperationAnalyse(OperationAnalyse o) {
        session.save(o);
        return true;
    }

    public boolean createFichePayement(FichePayement o) {
        session.save(o);
        session.close();
        return true;
    }



    @Override
    public boolean update(FicheConsultation o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(FicheConsultation o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public FicheConsultation findById(int id) {
        FicheConsultation ficheConsultation= null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        ficheConsultation = (FicheConsultation) session.get(FicheConsultation.class, id);
        session.getTransaction().commit();
        return ficheConsultation;
    }

    @Override
    public List<FicheConsultation> findAll() {
        session.beginTransaction();
        Query<FicheConsultation> ficheConsultationQuery= session.createQuery("from fiche_consultation");
        session.getTransaction().commit();
        return ficheConsultationQuery.list();
    }

    @Override
    public List<FicheConsultation> filterByDate(Date da) throws Exception {
        session.beginTransaction();
        Query<FicheConsultation> ficheConsultationQuery = session.createQuery("from consultation where date_consultation = da");
        ficheConsultationQuery.setParameter("da",da);
        session.getTransaction().commit();
        return ficheConsultationQuery.list();
    }

    @Override
    public List<FicheConsultation> filterByDatePatient(Date da, int id) throws Exception {
        session.beginTransaction();
        Query<FicheConsultation> ficheConsultationQuery = session.createQuery("from consultation where date_consultation = ?1 and id = ?2");
        ficheConsultationQuery.setParameter(1,da, DateType.INSTANCE);
        ficheConsultationQuery.setParameter(2,id, IntegerType.INSTANCE);
        session.getTransaction().commit();
        return ficheConsultationQuery.list();
    }




}
