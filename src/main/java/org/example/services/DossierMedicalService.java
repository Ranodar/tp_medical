package org.example.services;

import org.example.entities.DossierMedical;
import org.example.entities.Patient;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class DossierMedicalService extends BaseService implements IDAO<DossierMedical> {

    public DossierMedicalService(){
        super();
    }

    @Override
    public boolean create(DossierMedical o) {
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
//        session.getTransaction().commit();
//        session.close();
        return true;
    }

    public boolean createPatient(Patient o) {
//        session = sessionFactory.openSession();
//        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        session.close();
        return true;
    }

    @Override
    public boolean update(DossierMedical o) {
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(DossierMedical o) {
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public DossierMedical findById(int id) {
        DossierMedical dossierMedical = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        dossierMedical = (DossierMedical) session.get(DossierMedical.class, id);
        session.getTransaction().commit();
        return dossierMedical;
    }

    @Override
    public List<DossierMedical> findAll() {
        session.beginTransaction();
        Query<DossierMedical> dossierMedicalQuery = session.createQuery("from dossier_medical");
        session.getTransaction().commit();
        return dossierMedicalQuery.list();
    }

    @Override
    public List<DossierMedical> filterByDate(Date da) throws Exception {
        return null;
    }

    @Override
    public List<DossierMedical> filterByDatePatient(Date da, int id) throws Exception {
        return null;
    }


}
