package org.example.interfaces;

import java.util.Date;
import java.util.List;

public interface IDAO<T> {

    boolean create(T o);

    boolean update(T o);

    boolean delete(T o);

    T findById(int id);

    List<T> findAll();

    List<T> filterByDate(Date da) throws Exception;

    List<T> filterByDatePatient(Date da, int id) throws Exception;

}
