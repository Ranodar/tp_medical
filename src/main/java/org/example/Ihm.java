package org.example;


import org.example.entities.*;
import org.example.services.BaseService;
import org.example.services.DossierMedicalService;
import org.example.services.FicheConsultationService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Ihm {
    private FicheConsultationService ficheConsultationService;
    private DossierMedicalService dossierMedicalService;
    private Scanner scanner;

    public Ihm(){
        ficheConsultationService = new FicheConsultationService();
        dossierMedicalService = new DossierMedicalService();
        scanner = new Scanner(System.in);
    }

    public void start() throws ParseException {
        String choice;
        do {
            menu();
            choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    createDossierMedical();
                    break;
                case "2":
                    createConsultation();
                    break;
                case "3":
                    getDossierMedicalById();
                    break;
                case "4":
                    getConsultationByDateById();
                    break;
            }
        }while(!choice.equals("0"));
    }

    private void menu() {
        System.out.println("###########Menu###############");
        System.out.println("1 -- Créer un dossier médical");
        System.out.println("2 -- Créer une consultation");
        System.out.println("3 -- Afficher le dossier médical d'un patient");
        System.out.println("4 -- Rechercher une consultation par date et patient");
    }





    private void createDossierMedical() throws ParseException {

        System.out.println("Veuillez saisir les informations du patient.");
        System.out.println("Numéro de sécurité social : ");
        String nss = scanner.nextLine();
        System.out.println("Prénom : ");
        String prenom = scanner.nextLine();
        System.out.println("Nom : ");
        String nom = scanner.nextLine();
        System.out.println("Date de naissance au format dd/mm/yyyy : ");
        String dateNaissance = scanner.nextLine();
        Date dateNaissanceParse = new SimpleDateFormat("dd/mm/yyyy").parse(dateNaissance);
        System.out.println("Sexe (M/F) : ");
        char sexe = scanner.nextLine().charAt(0);
        System.out.println("Adresse : ");
        String adresse = scanner.nextLine();
        System.out.println("Numéro de téléphone : ");
        int numTel = scanner.nextInt();
        scanner.nextLine();
        Patient patient = new Patient(nss,prenom,nom,dateNaissanceParse,sexe,adresse,numTel);

        System.out.println("Veuillez saisir le code d'accès du nouveau dossier associé au patient : ");
        String codeAccess = scanner.nextLine();
        DossierMedical dossierMedical = new DossierMedical(codeAccess,patient);
        patient.setDossierMedical(dossierMedical);
        dossierMedicalService.create(dossierMedical);
        dossierMedicalService.createPatient(patient);


    }

    private void createConsultation() throws ParseException {

        FicheConsultation ficheConsultation = new FicheConsultation();
        System.out.println("Saisir la date de la consultation : ");
        String dateConsultation = scanner.nextLine();
        Date dateConsultationParse = new SimpleDateFormat("dd/mm/yyyy").parse(dateConsultation);
        System.out.println("Saisir l'heure de la consultation : ");
        String heure = scanner.nextLine();
        System.out.println("Saisir le lieu de la consultation : ");
        String lieu = scanner.nextLine();
        System.out.println("Consultation payée ? (O: oui, N:non)");
        char etatConsult = scanner.nextLine().charAt(0);
        Consultation consultation = new Consultation(dateConsultationParse,heure,lieu,etatConsult,ficheConsultation);
        //
        List<Prescription> prescriptions = new ArrayList<>();
        System.out.println("Saisir le nombre de prescription : ");
        int nb = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < nb; i++) {
            System.out.println("Designation de la prescription numéro "+i+" : ");
            String designation = scanner.nextLine();
            System.out.println("Saisir la période de prescription : ");
            String periode = scanner.nextLine();

        }

        //
        System.out.println("Saisir le compte-rendu de la consultation : ");
        String compteRendu = scanner.nextLine();
        ficheConsultation.setCompteRendu(compteRendu);






    }

    private void getDossierMedicalById(){

    }

    private void getConsultationByDateById(){

    }



}

