package org.example.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "fiche_de_soin")
public class FicheDeSoin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "numero_fiche")
    private int numeroFiche;

    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation = new Date();

    @Column(name = "agent_createur")
    private String agentCreateur;

    @Column(name = "adresse_createur")
    private String adresseCreateur;

    @OneToOne
    private FichePayement fichePayement;

    @OneToOne
    private FicheConsultation ficheConsultation;

    //constructors
    public FicheDeSoin() {
    }

    public FicheDeSoin(String agentCreateur, String adresseCreateur) {
        this.agentCreateur = agentCreateur;
        this.adresseCreateur = adresseCreateur;
    }

    //methodes
    public void getFicheDeSoin(){

    }

    //getters et setters
    public int getNumeroFiche() {
        return numeroFiche;
    }

    public void setNumeroFiche(int numeroFiche) {
        this.numeroFiche = numeroFiche;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAgentCreateur() {
        return agentCreateur;
    }

    public void setAgentCreateur(String agentCreateur) {
        this.agentCreateur = agentCreateur;
    }

    public String getAdresseCreateur() {
        return adresseCreateur;
    }

    public void setAdresseCreateur(String adresseCreateur) {
        this.adresseCreateur = adresseCreateur;
    }

    public FichePayement getFichePayement() {
        return fichePayement;
    }

    public void setFichePayement(FichePayement fichePayement) {
        this.fichePayement = fichePayement;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }


    //toString
    @Override
    public String toString() {
        return "FicheDeSoin{" +
                "numeroFiche=" + numeroFiche +
                ", dateCreation=" + dateCreation +
                ", agentCreateur='" + agentCreateur + '\'' +
                ", adresseCreateur='" + adresseCreateur + '\'' +
                ", fichePayement=" + fichePayement +
                ", ficheConsultation=" + ficheConsultation +
                '}';
    }
}
