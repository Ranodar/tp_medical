package org.example.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "fiche_consultation")
public class FicheConsultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "compte_rendu")
    private String compteRendu;

    @OneToOne
    private FicheDeSoin ficheDeSoin;

    @OneToOne
    private Consultation consultation;

    @OneToMany
    private List<Prescription> prescriptions;

    @OneToMany
    private List<OperationAnalyse> operationAnalyses;

    //constructors
    public FicheConsultation() {
    }

    public FicheConsultation(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    //methodes
    public List<Prescription> getPrescriptionList(){
        return prescriptions;
    }

    public List<OperationAnalyse> getAnalyses(){
        return operationAnalyses;
    }

    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompteRendu() {
        return compteRendu;
    }

    public void setCompteRendu(String compteRendu) {
        this.compteRendu = compteRendu;
    }

    public FicheDeSoin getFicheDeSoin() {
        return ficheDeSoin;
    }

    public void setFicheDeSoin(FicheDeSoin ficheDeSoin) {
        this.ficheDeSoin = ficheDeSoin;
    }

    public Consultation getConsultation() {
        return consultation;
    }

    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public List<OperationAnalyse> getOperationAnalyses() {
        return operationAnalyses;
    }

    public void setOperationAnalyses(List<OperationAnalyse> operationAnalyses) {
        this.operationAnalyses = operationAnalyses;
    }


    //toString
    @Override
    public String toString() {
        return "FicheConsultation{" +
                "id=" + id +
                ", compteRendu='" + compteRendu + '\'' +
                ", ficheDeSoin=" + ficheDeSoin +
                ", consultation=" + consultation +
                ", prescriptions=" + prescriptions +
                ", operationAnalyses=" + operationAnalyses +
                '}';
    }
}
