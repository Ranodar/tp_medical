package org.example.entities;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "dossier_medical")
public class DossierMedical {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int numero;

    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation = new Date();


    @Column(name = "code_access")
    private String codeAccess;


    @OneToOne
    private Patient patient;


    @OneToMany
    private List<FicheDeSoin> ficheDeSoins;


    //constructors
    public DossierMedical() {
    }

    public DossierMedical(String codeAccess, Patient patient) {
        this.codeAccess = codeAccess;
        this.patient = patient;
    }

    //methodes
    public void getAllFiches(){

    }


    //getters et setters
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCodeAccess() {
        return codeAccess;
    }

    public void setCodeAccess(String codeAccess) {
        this.codeAccess = codeAccess;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<FicheDeSoin> getFicheDeSoins() {
        return ficheDeSoins;
    }

    public void setFicheDeSoins(List<FicheDeSoin> ficheDeSoins) {
        this.ficheDeSoins = ficheDeSoins;
    }


    //toString
    @Override
    public String toString() {
        return "DossierMedical{" +
                "numero=" + numero +
                ", dateCreation=" + dateCreation +
                ", codeAccess='" + codeAccess + '\'' +
                ", patient=" + patient +
                ", ficheDeSoins=" + ficheDeSoins +
                '}';
    }
}
