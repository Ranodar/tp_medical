package org.example.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "fiche_payement")
public class FichePayement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "date_exigibilite")
    private Date dateExigibilite;

    @Column(name = "date_payement")
    private Date datePayement;

    @Column(name = "montant_paye")
    private double montantPaye;

    @Column(name = "indicateur_payement")
    private boolean indicateurPayement;


    @OneToOne
    private FicheDeSoin ficheDeSoin;


    //constructors
    public FichePayement() {
    }

    public FichePayement(Date dateExigibilite, Date datePayement, double montantPaye, boolean indicateurPayement) {
        this.dateExigibilite = dateExigibilite;
        this.datePayement = datePayement;
        this.montantPaye = montantPaye;
        this.indicateurPayement = indicateurPayement;
    }

    //methodes
    public void getMontantPaye(){

    }

    public void regularisePayement(){

    }

    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateExigibilite() {
        return dateExigibilite;
    }

    public void setDateExigibilite(Date dateExigibilite) {
        this.dateExigibilite = dateExigibilite;
    }

    public Date getDatePayement() {
        return datePayement;
    }

    public void setDatePayement(Date datePayement) {
        this.datePayement = datePayement;
    }

    public void setMontantPaye(double montantPaye) {
        this.montantPaye = montantPaye;
    }

    public boolean isIndicateurPayement() {
        return indicateurPayement;
    }

    public void setIndicateurPayement(boolean indicateurPayement) {
        this.indicateurPayement = indicateurPayement;
    }

    public FicheDeSoin getFicheDeSoin() {
        return ficheDeSoin;
    }

    public void setFicheDeSoin(FicheDeSoin ficheDeSoin) {
        this.ficheDeSoin = ficheDeSoin;
    }


    //toString
    @Override
    public String toString() {
        return "FichePayement{" +
                "id=" + id +
                ", dateExigibilite=" + dateExigibilite +
                ", datePayement=" + datePayement +
                ", montantPaye=" + montantPaye +
                ", indicateurPayement=" + indicateurPayement +
                ", ficheDeSoin=" + ficheDeSoin +
                '}';
    }
}
