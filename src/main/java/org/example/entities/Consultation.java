package org.example.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Consultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "date_consultation")
    private Date dateConsultation;

    private String heure;

    private String lieu;

    @Column(name = "etat_consult")
    private char etatConsult;

    @OneToOne
    private FicheConsultation ficheConsultation;


    //constructors
    public Consultation() {
    }

    public Consultation(Date dateConsultation, String heure, String lieu, char etatConsult, FicheConsultation ficheConsultation) {
        this.dateConsultation = dateConsultation;
        this.heure = heure;
        this.lieu = lieu;
        this.etatConsult = etatConsult;
        this.ficheConsultation = ficheConsultation;
    }

    //methodes
    public void demandeConsult(){

    }

    public void annuleConsult(){

    }


    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateConsultation() {
        return dateConsultation;
    }

    public void setDateConsultation(Date dateConsultation) {
        this.dateConsultation = dateConsultation;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public char getEtatConsult() {
        return etatConsult;
    }

    public void setEtatConsult(char etatConsult) {
        this.etatConsult = etatConsult;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }


    //toString
    @Override
    public String toString() {
        return "Consultation{" +
                "id=" + id +
                ", dateConsultation=" + dateConsultation +
                ", heure='" + heure + '\'' +
                ", lieu='" + lieu + '\'' +
                ", etatConsult=" + etatConsult +
                ", ficheConsultation=" + ficheConsultation +
                '}';
    }
}
