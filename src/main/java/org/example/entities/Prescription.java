package org.example.entities;

import javax.persistence.*;

@Entity
public class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String designation;

    private String periode;

    private String indication;

    @ManyToOne
    private FicheConsultation ficheConsultation;

    //constructors
    public Prescription() {
    }

    public Prescription(String designation, String periode, String indication) {
        this.designation = designation;
        this.periode = periode;
        this.indication = indication;
    }

    //methodes


    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }


    //toString
    @Override
    public String toString() {
        return "Prescription{" +
                "id=" + id +
                ", designation='" + designation + '\'' +
                ", periode='" + periode + '\'' +
                ", indication='" + indication + '\'' +
                ", ficheConsultation=" + ficheConsultation +
                '}';
    }
}
