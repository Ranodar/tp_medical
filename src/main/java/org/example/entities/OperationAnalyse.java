package org.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "operation_analyse")
public class OperationAnalyse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String description;

    @Column(name = "date_heure_operation")
    private Date dateHeureOperation;

    private String resultat;

    @ManyToOne
    private FicheConsultation ficheConsultation;

    //constructors
    public OperationAnalyse() {
    }

    public OperationAnalyse(String description, Date dateHeureOperation, String resultat) {
        this.description = description;
        this.dateHeureOperation = dateHeureOperation;
        this.resultat = resultat;
    }

    //methodes
    public void getResult(){
    }

    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateHeureOperation() {
        return dateHeureOperation;
    }

    public void setDateHeureOperation(Date dateHeureOperation) {
        this.dateHeureOperation = dateHeureOperation;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public FicheConsultation getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }


    //toString
    @Override
    public String toString() {
        return "OperationAnalyse{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", dateHeureOperation=" + dateHeureOperation +
                ", resultat='" + resultat + '\'' +
                ", ficheConsultation=" + ficheConsultation +
                '}';
    }
}
