package org.example.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nss;

    private String prenom;

    private String nom;

    @Column(name = "date_naissance")
    private Date dateNaissance;

    private char sexe;

    private String adresse;

    @Column(name = "num_telephone")
    private int numTelephone;


    @OneToOne
    private DossierMedical dossierMedical;



    //constructors
    public Patient() {
    }

    public Patient(String nss, String prenom, String nom, Date dateNaissance, char sexe, String adresse, int numTelephone) {
        this.nss = nss;
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.sexe = sexe;
        this.adresse = adresse;
        this.numTelephone = numTelephone;
    }

    //methodes
    public void getDossierMedical(){

    }

    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getNumTelephone() {
        return numTelephone;
    }

    public void setNumTelephone(int numTelephone) {
        this.numTelephone = numTelephone;
    }

    public void setDossierMedical(DossierMedical dossierMedical) {
        this.dossierMedical = dossierMedical;
    }


    //toString
    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", nss='" + nss + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", sexe=" + sexe +
                ", adresse='" + adresse + '\'' +
                ", numTelephone=" + numTelephone +
                ", dossierMedical=" + dossierMedical +
                '}';
    }
}
